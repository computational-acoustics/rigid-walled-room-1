# Rigid Walled Room 1

ElmerFEM model for the steady state field inside a rigid walled room.

Part of the [accompanying repositories](https://gitlab.com/computational-acoustics) of the [Computational Acoustics with Open Source Software](https://computational-acoustics.gitlab.io/website/) project.

## Covering Episodes

* [Rigid Walled Room](https://computational-acoustics.gitlab.io/website/posts/10-rigid-walled-room/).

## Study Summary

The main parameters of the study are reported below.

### Source and Medium

|Parameter Name               | Symbol       | Value                                             | Unit                      |
|-----------------------------|--------------|---------------------------------------------------|---------------------------|
| Source Radius               | $`a`$        | 0.005                                             | meters                    |
| Source Frequencies          | $`f`$        | 34.3 42.9 54.9 57.2 66.7 68.6 71.5 79.3 80.9 85.8 | hertz                     |
| Source Surface Displacement | $`d`$        | 0.001                                             | meters                    |
| Medium Sound Phase Speed    | $`c_{0}`$    | 343                                               | meters per second         |
| Medium Equilibrium Density  | $`\rho_{0}`$ | 1.205                                             | kilograms per cubic meter |

### Domain

| Shape            | Size             | Mesh Algorithm  | Mesh Min. Size | Mesh Max. Size   | Element Order | Source Location |
|------------------|------------------|-----------------|----------------|------------------|---------------|-----------------|
| Rectangular Room | 5 X 4 X 3 meters | NETGEN 1D-2D-3D | 1 millimetre   | 300 millimetres  | Second        | Room Corner     |

### Boundary Condition

Rigid walls.

## Software Overview

The table below reports the software used for this project.

| Software                                           | Usage                        |
|----------------------------------------------------|------------------------------|
| [FreeCAD](https://www.freecadweb.org/)             | 3D Modeller                  |
| [Salome Platform](https://www.salome-platform.org) | Pre-processing               |
| [ElmerFEM](http://www.elmerfem.org)                | Multiphysical solver         |
| [ParaView](https://www.paraview.org/)              | Post-processing              |
| [Julia](https://julialang.org/)                    | Technical Computing Language |

## Repo Structure

* `elmerfem` contains the ElmreFEM project.
* `geometry.FCStd` is the FreeCAD geometry model. This file can be used to export geometric entities to _BREP_ files to pre-process with Salome. _BREP_ files are excluded from the repo as they are redundant.
* `meshing.hdf` is the Salome study of the geometry. It contains the geometry pre-processing and meshing. The mesh is exported into `elmerfem` as `elmerfem/Mesh_1.unv`. Note that the mesh in this file is **not** computed.
* The folder `frequencies` contains the study frequencies as an array of `csv` files which is loadable in ParaView.
* `validate.jl` contains Julia code to compare the numerical FEM solution to the analytical one. Note that the code has to be edited to make it point to line data exported through ParaView. Also, this code is for guidance only and results might differ with different Elmer versions. Refer to the tutorial for more information.

The repo contains only the _source_ of the simulation. To obtain results, the study must be solved.

## How to Run this Study

Follow the steps below to run the study.

Clone the repository and `cd` into the cloned directory:

```bash
git clone https://gitlab.com/computational-acoustics/rigid-walled-room-1.git
cd rigid-walled-room-1/
```

`cd` in the `elmerfem` directory and run the study:

```bash
cd elmerfem/
ElmerSolver
```

When finished, open ParaView and select `File > Load State` to load the `.pvsm` file in the root of the repository. To load the ParaView state successfully, choose _Search files under specified directory_ as shown in the example below. The folder specified in `Data Directory` should be the root folder of the repo (blurred below as the absolute path will differ in your machine).

![Load State Example](res/pictures/paraview-load-state.png)

To be able to run `validate.jl` as is, first export the data from ParaView:

* Select the `case_t..vtue` object in the `Pipeline Browser`.
* Choose `File > Save Data`.
* Navigate inside the 'export' folder in the repository root directory and use `data` as a `File name`. Click `OK`.
* Use `30` as `Precision`, tick `Write Time Steps` and leave the default `File name suffix`. Click `OK`.

Your Julia environment must have the following packages installed:

* [Plots](http://docs.juliaplots.org/latest/);
* [JLD](https://github.com/JuliaIO/JLD.jl);
* [AcousticModels](https://gitlab.com/computational-acoustics/acousticmodels.jl).

At this point, you will be able to `cd` back to the repository root and run `validate.jl`:

```bash
cd ../
julia validate.jl
```

